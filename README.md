# Backend de Gestão de Contratos

Este é um projeto simples para o backend do Gestão de Contratos, ele executa com node.js utilizando [Express](https://www.npmjs.com/package/express) e [Mongoose](https://www.npmjs.com/package/mongoose) para operar o Banco de Dados Mongodb que é utilizado. Também são utilizadas ferramentas auxiliares tais como:

- [Bcrypt](https://www.npmjs.com/package/bcrypt)
- [Body Parser](https://www.npmjs.com/package/body-parser)
- [Cors](https://www.npmjs.com/package/cors)
- [Json Web Token](https://www.npmjs.com/package/jsonwebtoken)
- [Multer](https://www.npmjs.com/package/multer)

# Instalação

Para instalar todas as dependências basta executar:

```sh
npm install
```

# Setup do Bando de Dados

Para possibilitar a execução do servidor é preciso ter um banco MongoDB e configurar os dados de conexão em um arquivo que deve ser criado da parta raiz do projeto, com o nome config.json.

###

    .
    ├── public
    ├── src
    ├── config.json

A sugestão é criar o Banco utilizando a solução na nuvem do MongoDB. É possível criar pelo [link](https://www.mongodb.com/cloud/atlas/register).
O arquivo config.json deve ter o seguinte formato:

```json
{
  "token_secret": "CHAVE_SECRETA_PARA_GERACAO_DO_JWT",
  "mongo_uri": "mongodb+srv://SEU_USER:SUA_SENHA@cluster0-gp6jm.mongodb.net/SUA_COLECAO?retryWrites=true&w=majority"
}
```

Atente para o endereço do servidor Mongo pode alterar, mas após criar sua conta na Mongo Cloud, existe um passo a passo de como conectar, e essa URL será informada.
A chave TOKEN_SECRET é a chave secreta para geração e validação dos JWTs (JSON Web Tokens).

# Validação de usuários

O servidor requer um JWT válido para conectar por isso são disponibilizadas as rotas para criação de usuários e posterior login.
A validade dos tokens é de uma hora.
O identificador único do lado do servidor para os usuários é o E-mail, logo um usuário por e-mail.

# Iniciando o servidor

Para iniciar o servidor basta executar o comando:

```sh
npm start
```

O servidor vai escutar na porta 4000.

# Contribuição/Contato

Você pode me encontrar no Twitter [@edukandalski](https://twitter.com/edukandalski).
