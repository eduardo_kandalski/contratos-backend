const express = require('express');
const mongoose = require('mongoose');
const multer = require('multer');
const path = require('path');
const requireAuth = require('../middlewares/requireAuth');

const Contract = mongoose.model('Contract');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads');
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
  },
});

const upload = multer({ storage });

const router = express.Router();

router.use(requireAuth);

router.get('/contracts', async (req, res) => {
  const contracts = await Contract.find();

  res.send(contracts);
});

router.get('/contracts/:id', async (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);

  try {
    const contract = await Contract.findById(id);
    if (!contract) {
      return res.status(500).send({ error: 'Registro nao encontrado' });
    }

    res.send(contract);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.post('/contracts/upload', upload.single('file'), (req, res) => {
  res.send(req.file);
});

router.post('/contracts', async (req, res) => {
  const { title, begind, endd, filepath, parties } = req.body;

  let partiesIds = [];

  if (parties) {
    partiesIds = parties.map((party) => mongoose.Types.ObjectId(party));
  }

  try {
    const contract = new Contract({ title, begind, endd, filepath, parties: partiesIds });
    await contract.save();
    res.send(contract);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.put('/contracts/:id', async (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);
  const { title, begind, endd, parties } = req.body;
  let dataChanged = false;
  let partiesIds = [];

  try {
    const contract = await Contract.findById(id);

    if (parties) {
      partiesIds = parties.map((party) => mongoose.Types.ObjectId(party));
    } else {
      partiesIds = contract.parties;
    }

    if (!contract) {
      return res.status(500).send({ error: 'Registro nao encontrado' });
    }
    if (title && title !== contract.title) {
      contract.title = title;
      dataChanged = true;
    }
    if (begind && begind !== contract.begind) {
      contract.begind = begind;
      dataChanged = true;
    }
    if (endd && endd !== contract.endd) {
      contract.endd = endd;
      dataChanged = true;
    }
    if (partiesIds && partiesIds !== contract.parties) {
      contract.parties = partiesIds;
      dataChanged = true;
    }

    if (dataChanged) {
      await contract.save();
    }

    res.send(contract);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.delete('/contracts/:id', async (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);

  try {
    const contract = await Contract.findById(id);

    if (!contract) {
      return res.status(500).send({ error: 'Registro nao encontrado' });
    }

    await Contract.findByIdAndDelete(id);
    res.send();
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

module.exports = router;
