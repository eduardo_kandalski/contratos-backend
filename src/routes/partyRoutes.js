const express = require('express');
const mongoose = require('mongoose');
const requireAuth = require('../middlewares/requireAuth');
const { route } = require('./authRoutes');

const Party = mongoose.model('Party');

const router = express.Router();

router.use(requireAuth);

router.get('/parties', async (req, res) => {
  const parties = await Party.find();

  res.send(parties);
});

router.get('/parties/:id', async (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);

  try {
    const party = await Party.findById(id);
    if (!party) {
      return res.status(500).send({ error: 'Registro nao encontrado' });
    }
    res.send(party);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.post('/parties', async (req, res) => {
  const { name, surname, email, cpf, phone } = req.body;

  try {
    const party = new Party({ name, surname, email, cpf, phone });
    await party.save();
    res.send(party);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.put('/parties/:id', async (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);
  const { name, surname, email, cpf, phone } = req.body;
  let dataChanged = false;

  try {
    const party = await Party.findById(id);
    if (!party) {
      return res.status(500).send({ error: 'Registro nao encontrado' });
    }
    if (name && name !== party.name) {
      party.name = name;
      dataChanged = true;
    }
    if (surname && surname !== party.surname) {
      party.surname = surname;
      dataChanged = true;
    }
    if (email && email !== party.email) {
      party.email = email;
      dataChanged = true;
    }
    if (cpf && cpf !== party.cpf) {
      party.cpf = cpf;
      dataChanged = true;
    }
    if (phone && phone !== party.phone) {
      party.phone = phone;
      dataChanged = true;
    }

    if (dataChanged) {
      await party.save();
    }

    res.send(party);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.delete('/parties/:id', async (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);

  try {
    const party = await Party.findById(id);

    if (!party) {
      return res.status(500).send({ error: 'Registro nao encontrado' });
    }

    await Party.findByIdAndDelete(id);
    res.send();
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

module.exports = router;
