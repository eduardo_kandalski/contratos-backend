const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const User = mongoose.model('User');
const config = require('../../config.json');

const router = express.Router();

router.post('/signup', async (req, res) => {
  const { email, password, name, surname } = req.body;

  try {
    const user = new User({ email, password, name, surname });
    await user.save();

    const token = jwt.sign({ userId: user._id }, config.token_secret, { expiresIn: 3600 });
    res.send({ email: user.email, name: user.name, surname: user.surname, token });
  } catch (err) {
    return res.status(422).send({ error: err.message });
  }
});

router.post('/signin', async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(422).send({ error: 'Obrigatorio informar email e senha' });
  }

  const user = await User.findOne({ email });
  if (!user) {
    return res.status(422).send({ error: 'Senha ou e-mail incorreto' });
  }

  try {
    await user.comparePassword(password);
    const token = jwt.sign({ userId: user._id }, config.token_secret, { expiresIn: 3600 });
    res.send({ email: user.email, name: user.name, surname: user.surname, token });
  } catch (err) {
    return res.status(422).send({ error: 'Senha ou e-mail incorreto' });
  }
});

module.exports = router;
