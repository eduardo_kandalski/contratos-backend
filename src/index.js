require('./models/User');
require('./models/Party');
require('./models/Contract');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');
const authRoutes = require('./routes/authRoutes');
const partyRoutes = require('./routes/partyRoutes');
const contractRoutes = require('./routes/contractRoutes');
const requireAuth = require('./middlewares/requireAuth');
const config = require('../config.json');

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(authRoutes);
app.use(partyRoutes);
app.use(contractRoutes);

const mongoUri = config.mongo_uri;
mongoose.connect(mongoUri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});

mongoose.connection.on('connected', () => {
  console.log('Conectado a instancia mongodb');
});
mongoose.connection.on('error', (err) => {
  console.error('Erro ao conectar no mongodb', err);
});

app.get('/', requireAuth, (req, res) => {
  res.send({ message: `Olá ${req.user.email}!` });
});

app.get('/download/:filename', (req, res) => {
  res.download(`${path.join(__dirname, `../public/uploads/${req.params.filename}`)}`);
});

app.listen(4000, () => {
  console.log('Servidor aguardando na porta 4000...');
});
