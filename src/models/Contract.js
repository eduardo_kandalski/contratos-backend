const mongoose = require('mongoose');

const contractSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  begind: {
    type: String,
    required: true,
  },
  endd: {
    type: String,
    required: true,
  },
  filepath: {
    type: String,
  },
  parties: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Party' }],
});

mongoose.model('Contract', contractSchema);
